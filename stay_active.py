import pyautogui
import time
import random
pyautogui.PAUSE = 1
pyautogui.FAILSAFE = True

#requires two application windows open
try:
    while True:
        pyautogui.keyDown('altleft')
        pyautogui.keyDown('tab')
        pyautogui.keyUp('tab')
        pyautogui.keyUp('altleft')
        time.sleep(random.randrange(0, 194))
except KeyboardInterrupt:
    pass